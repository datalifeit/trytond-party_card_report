# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.report import Report


class PartyCard(Report):
    """Party Card Report"""
    __name__ = 'party.card_report'
