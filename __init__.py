# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .party import PartyCard


def register():
    Pool.register(
        PartyCard,
        module='party_card_report', type_='report')
