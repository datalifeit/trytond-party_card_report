# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.pool import Pool


class PartyCardReportTestCase(ModuleTestCase):
    """Test Party Card Report module"""
    module = 'party_card_report'

    @with_transaction()
    def test0010party_card_report(self):
        'Test party label report'
        pool = Pool()
        Party = pool.get('party.party')
        report = pool.get('party.card_report', type='report')

        party_card_report1, = Party.create([{
            'name': 'Party 1',
            }])
        oext, content, _, _ = report.execute([party_card_report1.id], {})
        self.assertEqual(oext, 'odt')
        self.assertTrue(content)


del ModuleTestCase
